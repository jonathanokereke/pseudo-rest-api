package de.uniklinik.medic.pseudo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Jonathan on 31.May.2020 . 2:54 PM
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReadPatientTokenRequest {

    private String type;
    private ReadPatientTokenRequestData data;

    public ReadPatientTokenRequest() {

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ReadPatientTokenRequestData getData() {
        return data;
    }

    public void setData(ReadPatientTokenRequestData data) {
        this.data = data;
    }
}
