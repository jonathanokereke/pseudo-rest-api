package de.uniklinik.medic.pseudo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Jonathan on 31.May.2020 . 3:50 PM
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReadPatientTokenResponse {

    private String tokenId;
    private String tokenType;
    //private String uri;


    public ReadPatientTokenResponse() {
    }

    public ReadPatientTokenResponse(String tokenId, String tokenType) {
        this.tokenId = tokenId;
        this.tokenType = tokenType;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }


}
