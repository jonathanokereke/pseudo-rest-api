package de.uniklinik.medic.pseudo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by Jonathan on 03.Jun.2020 . 12:05 PM
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReadPatientIdReponse {

    public ReadPatientIdReponse() {
    }

    private Fields fields;
    private List<Id> ids = null;

    public Fields getFields() {
        return fields;
    }

    public void setFields(Fields fields) {
        this.fields = fields;
    }

    public List<Id> getIds() {
        return ids;
    }

    public void setIds(List<Id> ids) {
        this.ids = ids;
    }


    public static class Fields {

        private String geburtsname;
        private String vorname;
        private String nachname;

        public String getGeburtsname() {
            return geburtsname;
        }

        public void setGeburtsname(String geburtsname) {
            this.geburtsname = geburtsname;
        }

        public String getVorname() {
            return vorname;
        }

        public void setVorname(String vorname) {
            this.vorname = vorname;
        }

        public String getNachname() {
            return nachname;
        }

        public void setNachname(String nachname) {
            this.nachname = nachname;
        }


    }


    public static class Id {

        private String idType;
        private String idString;
        private Boolean tentative;

        public String getIdType() {
            return idType;
        }

        public void setIdType(String idType) {
            this.idType = idType;
        }

        public String getIdString() {
            return idString;
        }

        public void setIdString(String idString) {
            this.idString = idString;
        }

        public Boolean getTentative() {
            return tentative;
        }

        public void setTentative(Boolean tentative) {
            this.tentative = tentative;
        }

    }


}



