package de.uniklinik.medic.pseudo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
* Created by Jonathan on 31.May.2020 . 7:26 PM
*/

@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchId {

   private String idType;
   private String idString;

    public SearchId() {
    }

    public SearchId(String idType, String idString) {
        this.idType = idType;
        this.idString = idString;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdString() {
        return idString;
    }

    public void setIdString(String idString) {
        this.idString = idString;
    }


}
