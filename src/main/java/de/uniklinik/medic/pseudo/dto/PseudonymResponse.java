package de.uniklinik.medic.pseudo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Jonathan on 26.Apr.2020 . 8:53 PM
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PseudonymResponse {

    private String idType;
    private String idString;
    private String tentative;


    public PseudonymResponse() {
    }

    public PseudonymResponse(String idType, String idString, String tentative) {
        this.idType = idType;
        this.idString = idString;
        this.tentative = tentative;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdString() {
        return idString;
    }

    public void setIdString(String idString) {
        this.idString = idString;
    }

    public String getTentative() {
        return tentative;
    }

    public void setTentative(String tentative) {
        this.tentative = tentative;
    }

}
