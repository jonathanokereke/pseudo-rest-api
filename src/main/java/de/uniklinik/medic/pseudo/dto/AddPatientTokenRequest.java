package de.uniklinik.medic.pseudo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Jonathan on 25.Apr.2020 . 6:23 PM
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddPatientTokenRequest {

    private String type;
    private AddPatientTokenRequestData data;

    public AddPatientTokenRequest() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public AddPatientTokenRequestData getData() {
        return data;
    }

    public void setData(AddPatientTokenRequestData data) {
        this.data = data;
    }
}
