package de.uniklinik.medic.pseudo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Jonathan on 25.Apr.2020 . 6:10 PM
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddPatientTokenResponse {

    private String tokenId;
    private String tokenType;

    public AddPatientTokenResponse() {
    }

    public AddPatientTokenResponse(String tokenId, String tokenType) {
        this.tokenId = tokenId;
        this.tokenType = tokenType;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }
}
