package de.uniklinik.medic.pseudo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by Jonathan on 31.May.2020 . 2:56 PM
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReadPatientTokenRequestData {

    private List<SearchId> searchIds;
    private String[] resultFields;
    private String[] resultIds;

    public ReadPatientTokenRequestData() {
    }

    public List<SearchId> getSearchIds() {
        return searchIds;
    }

    public void setSearchIds(List<SearchId> searchIds) {
        this.searchIds = searchIds;
    }

    public String[] getResultFields() {
        return resultFields;
    }

    public void setResultFields(String[] resultFields) {
        this.resultFields = resultFields;
    }

    public String[] getResultIds() {
        return resultIds;
    }

    public void setResultIds(String[] resultIds) {
        this.resultIds = resultIds;
    }
}

