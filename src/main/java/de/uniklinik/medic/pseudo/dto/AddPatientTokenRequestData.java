package de.uniklinik.medic.pseudo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Jonathan on 25.Apr.2020 . 6:30 PM
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class AddPatientTokenRequestData {

    private String[] idtypes;

    public AddPatientTokenRequestData() {
    }

    public String[] getIdtypes() {
        return idtypes;
    }

    public void setIdtypes(String[] idtypes) {
        this.idtypes = idtypes;
    }
}
