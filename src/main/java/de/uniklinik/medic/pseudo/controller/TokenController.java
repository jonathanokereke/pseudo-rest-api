package de.uniklinik.medic.pseudo.controller;

import de.uniklinik.medic.pseudo.dto.*;
import de.uniklinik.medic.pseudo.entity.Token;
import de.uniklinik.medic.pseudo.repository.TokenRepository;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Created by Jonathan on 25.Apr.2020 . 6:17 PM
 */

@RestController
@RequestMapping("/api/v2/pseudo-service")
public class TokenController {

    @Value("${server.host}")
    private String serverHost;

    @Value("${server.mainzellisteApiKey}")
    private String mainzellisteApiKey;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private MessageSource messageSource;

    //CREATE TOKEN
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/sessions/{sessionId}/tokens", method = RequestMethod.POST)
    public Object createAddPatientToken(@PathVariable("sessionId") String sessionId) {

        //TODO only for testing - refactor with Handler
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        httpHeaders.set("mainzellisteApiKey", mainzellisteApiKey);

        String[] values = {"pid", "intid"};
        String tokenType = "addPatient";

        AddPatientTokenRequestData data = new AddPatientTokenRequestData();
        data.setIdtypes(values);

        AddPatientTokenRequest addPatientTokenRequest = new AddPatientTokenRequest();
        addPatientTokenRequest.setType(tokenType);
        addPatientTokenRequest.setData(data);

        HttpEntity<AddPatientTokenRequest> httpEntity = new HttpEntity<>(addPatientTokenRequest, httpHeaders);

        //Check if session is valid
        try{

            restTemplate.exchange(
                    serverHost+"/sessions/"+sessionId,
                    HttpMethod.GET,
                    httpEntity,
                    String.class
            ).getStatusCode();

            //If no Error is caught, then continue...

            String jsonToken = restTemplate.exchange(
                    serverHost+"/sessions/" + sessionId + "/tokens",
                    HttpMethod.POST,
                    httpEntity,
                    String.class
            ).getBody();

            try {
                JSONObject jsonObject = (JSONObject) new JSONParser().parse(jsonToken);

                String tokenId = (String) jsonObject.get("tokenId");

                //Persist token in DB
                Token token = new Token();
                token.setTokenId(tokenId);
                token.setType(tokenType);
                token.setCreatedAt(Instant.now().getEpochSecond());

                tokenRepository.save(token);

                return new AddPatientTokenResponse(tokenId, tokenType);
            }
            //TODO Handle exception here better
            catch (ParseException ex) {
                ex.printStackTrace();
            }

        }
        catch (HttpClientErrorException | HttpServerErrorException HttpError)
        {
            return new ResponseEntity<>(messageSource.getMessage("session.notFound", null, Locale.ENGLISH), HttpStatus.NOT_FOUND);
        }

        return null;

    }

    //CREATE TOKEN - Read Patient
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value = "/sessions/{sessionId}/tokens/ids/pid", method = RequestMethod.POST)
    public Object createReadPatientToken(
            @RequestParam String pid,
            @PathVariable("sessionId") String sessionId ){

        List<SearchId> searchIds = new ArrayList<SearchId>();
        searchIds.add(new SearchId("pid", pid));

        String[] resultFields = {"vorname", "nachname", "geburtsname"};
        //TODO: Add other ids (ext.id, InsuranceId etc.)
        String[] resultIds = {"pid", "intid"}; //intid will not be returned, we will strictly use this for internal troubleshooting of the Mainzelliste server DB

        String tokenType = "readPatients";

        ReadPatientTokenRequestData readPatientTokenRequestData = new ReadPatientTokenRequestData();
        readPatientTokenRequestData.setSearchIds(searchIds);
        readPatientTokenRequestData.setResultFields(resultFields);
        readPatientTokenRequestData.setResultIds(resultIds);

        ReadPatientTokenRequest readPatientTokenRequest = new ReadPatientTokenRequest();
        readPatientTokenRequest.setType(tokenType);
        readPatientTokenRequest.setData(readPatientTokenRequestData);

        //TODO only for testing - refactor with Handler
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        httpHeaders.set("mainzellisteApiKey", mainzellisteApiKey);

        HttpEntity<ReadPatientTokenRequest> httpEntity = new HttpEntity<>(readPatientTokenRequest, httpHeaders);

        //Check if session is valid
        try{

            restTemplate.exchange(
                    serverHost+"/sessions/"+sessionId,
                    HttpMethod.GET,
                    httpEntity,
                    String.class
            ).getStatusCode();

            //If no Error is caught, then continue...

            String jsonToken = restTemplate.exchange(
                    serverHost+"/sessions/" + sessionId + "/tokens",
                    HttpMethod.POST,
                    httpEntity,
                    String.class
            ).getBody();

            try {
                JSONObject jsonObject = (JSONObject) new JSONParser().parse(jsonToken);

                String tokenId = (String) jsonObject.get("tokenId");

                //Persist token in DB
                Token token = new Token();
                token.setTokenId(tokenId);
                token.setType(tokenType);
                token.setCreatedAt(Instant.now().getEpochSecond());

                tokenRepository.save(token);

                return new ReadPatientTokenResponse(tokenId, tokenType);
            }
            //TODO Handle exception here better
            catch (ParseException ex) {
                ex.printStackTrace();
            }

        }
        catch (HttpClientErrorException | HttpServerErrorException HttpError){

            HttpError.printStackTrace();

            //return new ResponseEntity<>(messageSource.getMessage("session.notFound", null, Locale.ENGLISH), HttpStatus.NOT_FOUND);
        }

        return null;



    }
}
