package de.uniklinik.medic.pseudo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Jonathan on 21.May.2020 . 6:13 PM
 */

@Configuration
@ConfigurationProperties(prefix = "server")
public class ConfigProperties {

    private String host;
    private String serviceId;
    private double mainzellisteApiVersion;
    private String mainzellisteApiKey;


    public ConfigProperties() {
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public double getMainzellisteApiVersion() {
        return mainzellisteApiVersion;
    }

    public void setMainzellisteApiVersion(double mainzellisteApiVersion) {
        this.mainzellisteApiVersion = mainzellisteApiVersion;
    }

    public String getMainzellisteApiKey() {
        return mainzellisteApiKey;
    }

    public void setMainzellisteApiKey(String mainzellisteApiKey) {
        this.mainzellisteApiKey = mainzellisteApiKey;
    }
}
